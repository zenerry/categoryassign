<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Categoryassign extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'categoryassign';
        $this->tab = 'administration';
        $this->version = '1.0.1';
        $this->author = 'ARDStudio';
        $this->need_instance = 0;
        $this->module_key = 'd8956b64e671ec3fcfb0ebf543481f9c';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Massive Products Category Reassignment');
        $this->description = $this->l('Append products from a category to another');

        $this->confirmUninstall = $this->l('Are you sure?');

        $this->ps_versions_compliancy = array('min' => '1.7.5', 'max' => '1.7.6.5');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('CATEGORYASSIGN_CATEGORY_FROM', false);
        Configuration::updateValue('CATEGORYASSIGN_CATEGORY_TO', false);
        Configuration::updateValue('CATEGORYASSIGN_DETACH', false);

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('BackOfficeHeader') &&
            $this->registerHook('displayBackOfficeCategory');
    }

    public function uninstall()
    {
        Configuration::deleteByName('CATEGORYASSIGN_CATEGORY_FROM');
        Configuration::deleteByName('CATEGORYASSIGN_CATEGORY_TO');
        Configuration::deleteByName('CATEGORYASSIGN_DETACH');

        return parent::uninstall();
    }


    /**
     * Load the configuration form
     */
    
    public function getContent()
    {

        /**
         * If values have been submitted in the form, process.
         */
        if (empty($messageOk)){ $messageOk = '';}
        if (empty($messageWarn)){ $messageWarn = '';}
        if (empty($messageDel)){ $messageDel = '';}

        if (((bool)Tools::isSubmit('submitCategoryassignModule'))) {
            $errors = $this->postProcess();
            $messageWarn = $errors['messageWarn'];
            $messageOk = $errors['messageOk'];
            $messageDel = $errors['messageDel'];
        }
        
        $this->context->smarty->assign(array(
            'module_dir', $this->_path,
            'messageOk' => $messageOk,
            'messageWarn' => $messageWarn,
            'messageDel' => $messageDel,
        ));

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        
            return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitCategoryassignModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        
        $helper->tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */

    protected function getConfigForm()
    {

        $root = Category::getRootCategory();

        //Generating the tree for the ORIGIN CAT
        $tree = new HelperTreeCategories('categories_from'); //The string in param is the ID used by the generated tree
        $tree->setUseCheckBox(false)
            ->setAttribute('is_category_filter', $root->id)
            ->setRootCategory($root->id)
            // ->setSelectedCategories(array((int)Configuration::get('CATEGORYASSIGN_CATEGORY_FROM')))
            ->setInputName('CATEGORYASSIGN_CATEGORY_FROM'); //Set the name of input. The option "name" of $fields_form doesn't seem to work with "categories_select" type
        $categoryTreeFrom = $tree->render();
        
        //Generating the tree for the DESTINATION CAT
        $tree = new HelperTreeCategories('categories_to');
        $tree->setUseCheckBox(false)
             ->setAttribute('is_category_filter', $root->id)
             ->setRootCategory($root->id)
            //  ->setSelectedCategories(array((int)Configuration::get('CATEGORYASSIGN_CATEGORY_TO')))
             ->setInputName('CATEGORYASSIGN_CATEGORY_TO');
        $categoryTreeTo = $tree->render();

        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type'  => 'categories_select',
                        'label' => $this->l('Origin Category'),
                        'desc' => $this->l('Select only ONE Category to select products FROM'),
                        'name'  => 'CATEGORYASSIGN_CATEGORY_FROM', //No ho podem treure si no, no passa la variable al configuration
                        'category_tree'  => $categoryTreeFrom, //This is the category_tree called in form.tpl
                        'required' => true

                     ),

                     array(
                        'type'  => 'categories_select',
                        'label' => $this->l('Destination Category'),
                        'desc' => $this->l('Select only ONE Category where to move/attach products TO'),
                        'name'  => 'CATEGORYASSIGN_CATEGORY_TO', //No ho podem treure si no, no passa la variable al configuration
                        'category_tree'  => $categoryTreeTo, //This is the category_tree called in form.tpl
                        'required' => true

                     ),

                    array(
                        'type' => 'switch',
                        'desc' => $this->l('Detach products from ORIGIN category'),
                        'name' => 'CATEGORYASSIGN_DETACH',
                        'label' => $this->l('Detach from origin?'),
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Submit'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'CATEGORYASSIGN_CATEGORY_FROM' => Configuration::get(('CATEGORYASSIGN_CATEGORY_FROM'), true),
            'CATEGORYASSIGN_CATEGORY_TO' => Configuration::get(('CATEGORYASSIGN_CATEGORY_TO'), true),
            'CATEGORYASSIGN_DETACH' => Configuration::get(('CATEGORYASSIGN_DETACH'), true), //Ojo que el True anava comentat
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }

        $errors = $this->sqlCategoryReassign();
        $messageOk = $errors['messageOk'];
        $messageWarn = $errors['messageWarn'];
        $messageDel = $errors['messageDel'];

        $detachActive = Configuration::get('CATEGORYASSIGN_DETACH');

        $articlesDiscount = Db::getInstance()->executeS('
            SELECT SP.id_product, CP.id_category  
            FROM ' . _DB_PREFIX_ . 'specific_price AS SP
            LEFT JOIN ' . _DB_PREFIX_ . 'category_product AS CP
            ON SP.id_product = CP.id_product
            WHERE CP.id_category = ' . $form_values['CATEGORYASSIGN_CATEGORY_FROM'] . ' AND SP.reduction <> "0.000000" ');

        $catFrom = new Category(Configuration::get('CATEGORYASSIGN_CATEGORY_FROM'), (int)$this->context->language->id);

        if ($detachActive == 1) {
            $form_values = $this->getConfigFormValues();
    // --- Si tenim  opció "DETACH" activat, escrivim el nou i borrem registre origen
            foreach ($articlesDiscount as $article) {
                $article = Db::getInstance()->execute('
                DELETE FROM '. _DB_PREFIX_. 'category_product 
                WHERE id_product = ' . (int)$article['id_product'] . '
                AND id_category = ' . (int)$form_values['CATEGORYASSIGN_CATEGORY_FROM']);
            }
            $messageDel = $this->displayError($this->l('Products successfully DETACHED from') . ' ' . $catFrom->name, array(), '');
        }

        return array(
            'messageOk' => $messageOk,
            'messageWarn' => $messageWarn,
            'messageDel' => $messageDel,
        );
    }

    protected function sqlCategoryReassign()
    {
        $form_values = $this->getConfigFormValues();

        $catFrom = new Category(Configuration::get('CATEGORYASSIGN_CATEGORY_FROM'), (int)$this->context->language->id);
        $catTo = new Category(Configuration::get('CATEGORYASSIGN_CATEGORY_TO'), (int)$this->context->language->id);

        // --- SQL per trobar productes amb descompte ---
        $articlesDiscount = Db::getInstance()->executeS('
            SELECT SP.id_product, CP.id_category  
            FROM ' . _DB_PREFIX_ . 'specific_price AS SP
            LEFT JOIN ' . _DB_PREFIX_ . 'category_product AS CP
            ON SP.id_product = CP.id_product
            WHERE CP.id_category = ' . $form_values['CATEGORYASSIGN_CATEGORY_FROM'] . ' AND SP.reduction <> "0.000000" ');
        
        if (count($articlesDiscount) > 0) {
            foreach ($articlesDiscount as $article) {
            // --- Si categoria destí = JA EXISTEIX, passem al següent
                if ($article[] = Db::getInstance()->executeS('
                    SELECT id_product, id_category
                    FROM ' . _DB_PREFIX_ . 'category_product
                    WHERE id_product = ' . (int)($article['id_product']) . '
                    AND id_category = ' . $form_values['CATEGORYASSIGN_CATEGORY_TO'])) {
                    $messageWarn = $this->displayWarning($this->l('Products where already in this category') . ' ' . $catFrom->name, array(), '');
                } else {
            // --- Si no trobem registre escrivim el nou
                    $article[] = Db::getInstance()->insert('category_product', array(
                        'id_product' => (int) $article['id_product'],
                        'id_category' => (int) ($form_values['CATEGORYASSIGN_CATEGORY_TO']),
                    ));
                    $messageOk = $this->displayConfirmation($this->l('Products successfully attached to') . ' ' . $catTo->name, array(), '');
                }
            }
        } else {
            $messageWarn = $this->displayWarning($this->l('No articles found'), array(), '');
        }

        return array (
            'messageOk' => $messageOk,
            'messageWarn' => $messageWarn,
//            'messageDel' => $messageDel,
        );
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayBackOfficeCategory()
    {
        /* Place your code here. */
    }
}
