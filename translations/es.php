<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{categoryassign}prestashop>categoryassign_4ad435811b7f1931146dcef84b1334af'] = 'Reasignación de categoría de productos, en masa';
$_MODULE['<{categoryassign}prestashop>categoryassign_c0b43ed8da0b95cbb7f517eda6cfea7d'] = 'Reasigna productos con descuento, de una categoría a otra';
$_MODULE['<{categoryassign}prestashop>categoryassign_729a51874fe901b092899e9e8b31c97a'] = '¿Estás seguro?';
$_MODULE['<{categoryassign}prestashop>categoryassign_f4f70727dc34561dfde1a3c529b6205c'] = 'Configuración';
$_MODULE['<{categoryassign}prestashop>categoryassign_174421436e76175a9a1e588ffb4fa1c3'] = 'Categoría de Orígen';
$_MODULE['<{categoryassign}prestashop>categoryassign_af22efaf0de756ef72eea077d7098717'] = 'Escoge la categoría desde la cual mover/asignar los productos';
$_MODULE['<{categoryassign}prestashop>categoryassign_965475e34500f04b6eeb2dc024681c37'] = 'Categoría de Destino';
$_MODULE['<{categoryassign}prestashop>categoryassign_21de2638b13e74ccccc8f72e3ba4d380'] = 'Escoge la categoría hacia donde mover o asignar los productos';
$_MODULE['<{categoryassign}prestashop>categoryassign_f4a67da2173f29d14ebc716a1adc1f6b'] = 'Desvincular de la categoría de ORIGEN';
$_MODULE['<{categoryassign}prestashop>categoryassign_840e3816a16c9f897af56e94e5707e85'] = 'Desvincular de orígen?';
$_MODULE['<{categoryassign}prestashop>categoryassign_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Habilitado';
$_MODULE['<{categoryassign}prestashop>categoryassign_b9f5c797ebbf55adccdd8539a65a0241'] = 'Deshabilitado';
$_MODULE['<{categoryassign}prestashop>categoryassign_a4d3b161ce1309df1c4e25df28694b7b'] = 'Enviar';
$_MODULE['<{categoryassign}prestashop>categoryassign_bd7ede31deabfab623bc60c5a1c84e63'] = 'Productos desvinculados con éxito de';
$_MODULE['<{categoryassign}prestashop>categoryassign_6449203cb51c576951d20908bceb7063'] = 'Algunos productos ya estaban en esta categoría';
$_MODULE['<{categoryassign}prestashop>categoryassign_30642bcea8a06dd93d2f48de8bae0065'] = 'Productos añadidos correctamente a';
$_MODULE['<{categoryassign}prestashop>categoryassign_39b031f1a8505a7556fadf4cd39ca3fd'] = 'No se han encontrado artículos';
$_MODULE['<{categoryassign}prestashop>configure_4ad435811b7f1931146dcef84b1334af'] = 'Reasignación de categoría masiva de productos ';
$_MODULE['<{categoryassign}prestashop>configure_64b615a31c9f9b33920943188240387b'] = 'Puedes mover o asignar productos de una categoría a otra, masivamente.';
$_MODULE['<{categoryassign}prestashop>configure_430442fa6d8d7240f9d4d7ba0c4a43e6'] = 'Escoge la categoría orígen desde la que mover/asignar los productos.';
$_MODULE['<{categoryassign}prestashop>configure_e6cb09ba27e37d1357fa47d428f03859'] = 'Escoge la categoría destino a la que los productos van a ser movidos o asignados.';
$_MODULE['<{categoryassign}prestashop>configure_69872b3657b2f4895641bef48b128867'] = 'Habilita esta opción para desvincular de la categoría de orígen.';
