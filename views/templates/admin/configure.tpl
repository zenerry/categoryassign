{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
	<h3><i class="icon icon-credit-card"></i> {l s='Massive Products Category Reassignment' mod='categoryassign'}</h3>
	<p>
		<strong>{l s='You can move or append products from a category to another one, massively' mod='categoryassign'}</strong><br />
		<br>
		{l s='Select an origin category from which you want product to be selected.' mod='categoryassign'}<br />
<br>
		{l s='Select destination category for products to be attached to.' mod='categoryassign'}<br />
<br>
		{l s='Switch ON for detaching products from origin category.' mod='categoryassign'}<br />
</p>
	{$messageDel}
	{$messageOk}
	{$messageWarn}
	
</div>